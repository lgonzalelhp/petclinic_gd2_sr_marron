package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class PetTest {
	
	public static Pet pet1;
	
	@BeforeClass
	public static void initClass() {
		pet1 = new Pet();
		pet1.setName("Mascota");
		pet1.setComments("Comentarios");
	}
	
	@Test
	public void testPet1() {
		assertEquals(pet1.getComments(), "Comentarios");
	}

}
