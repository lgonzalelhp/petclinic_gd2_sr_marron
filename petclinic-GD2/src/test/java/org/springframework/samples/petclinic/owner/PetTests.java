package org.springframework.samples.petclinic.owner;
/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class PetTests {
	public static PetType petType;
	public static Pet pet;
	public static Owner owner;
	
	public static Pet pet1;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		petType = new PetType();
		petType.setName("Perro");

		owner = new Owner();
		owner.setFirstName("Javier");
		owner.setLastName("Leon");
		owner.setAddress("Italia");
		owner.setCity("Espania");
		owner.setTelephone("6752134234");
					
		pet = new Pet();
		pet.setOwner(owner);
		pet.setName("Kira");
		pet.setBirthDate(LocalDate.now());	
		pet.setType(petType);
		
		owner.addPet(pet);
	}
	
	
	@Test
	public void testComments() {
		assertNull(pet.getComments());
		pet.setComments("Comentario de prueba");
		assertNotNull(pet.getComments());
		assertEquals(pet.getComments(), "Comentario de prueba");		
	}
	
	@Test
	public void testWeight() {
		assertEquals(pet.getWeight(), 0.0 , 0);	
		pet.setWeight(23);
		assertNotNull(pet.getWeight());
		assertEquals(pet.getWeight(), 23 , 0);			
	}	
	
	@AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	pet = null;
    	owner = null;
    }
	
}